<?php

namespace Drupal\drush_tools\Commands;

use Drush\Commands\DrushCommands;

/**
 * Debug Access Problems with drush commands.
 */
class AccessDebugCommands extends DrushCommands {

  /**
   * Debug access checks.
   *
   * @param string $route_name
   *   The route name to debug.
   * @param string $user_name
   *   The name of the account to debug the access
   *   for.
   * @param string $parameters
   *   The parameters (json) to pass to the route. eg {node:108}.
   *
   * @command drush_tools:access-debug
   * @aliases dtad
   *
   * @usage drush_tools-access-debug route.name route_params user_id
   *   Check if a certain user may access a certain route with certain
   *   parameters. For example to check if user someuser may edit the node
   *   with the nid 108 use drush dtad entity.node.edit_form '{"node":108}
   *   someuser
   */
  public function accessDebug(
    string $route_name,
    string $user_name,
    string $parameters = ''
  ) {
    // Convert paramters to array.
    $parameters = (array) json_decode($parameters);

    // Load drupal user account by uid.
    $account = user_load_by_name($user_name);
    $access_manager = \Drupal::service('access_manager');

    // Check if the user may access the route with the given parameters.
    $access_result = $access_manager->checkNamedRoute(
      $route_name,
      $parameters,
      $account,
      TRUE
    );
    $this->output()
      ->writeln($access_result->isAllowed() ? 'Allowed' : 'Not allowed');
    if (!$access_result->isAllowed()) {
      $this->output()->writeln($access_result->getReason());
    }

  }

  /**
   * Drush Command to get route name by path.
   *
   * @param string $path
   *   The path to get the route name for.
   *
   * @command drush_tools:get-route-name
   * @aliases dtgrn
   *
   * @usage drush dtgrn /node/1/edit
   */
  public function getRouteNameByPath($path) {
    $router = \Drupal::service('router.no_access_checks');
    try {
      $route_match = $router->match($path);
    }
    catch (Exception $e) {
      $this->output()->writeln('No route found for path ' . $path);
      return;
    }

    $this->output()->writeln('Route name:');
    $this->output()->writeln($route_match['_route']);

    if (isset($route_match['_raw_variables'])) {
      $json_parameters = json_encode($route_match['_raw_variables']->all());
      $this->output()->writeln('Parameters:');
      $this->output()->writeln($json_parameters);
    }

  }

}
