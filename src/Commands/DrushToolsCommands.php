<?php

namespace Drupal\drush_tools\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DrushToolsCommands extends DrushCommands {

  /**
   * Remove Duplicate URL aliases
   *
   * @param       $content_type
   *   Specify a content type for which the duplicate aliases should be
   *   removed.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option  option-name
   *   Description
   * @usage   drush_tools-remove-duplicate-aliases foo
   *   Usage description
   *
   * @command drush_tools:remove-duplicate-aliases
   * @aliases rda
   */
  public function commandName(
    $content_type = "none",
    $options = ['option-name' => 'default']
  ) {
    $query = \Drupal::entityQuery('node');
    if ("none" != $content_type) {
      $query->condition('type', $content_type, '=');
    }
    $nids = $query->execute();
    $path_alias_storage = \Drupal::entityTypeManager()->getStorage(
      'path_alias'
    );
    $languages = \Drupal::languageManager()->getLanguages();

    foreach ($nids as $nid) {
      foreach ($languages as $langcode => $language) {
        $actual_alias = \Drupal::service('path_alias.manager')
          ->getAliasByPath('/node/' . $nid, $langcode);

        // Load all path alias for this node.
        $alias_objects = $path_alias_storage->loadByProperties([
          'path' => '/node/' . $nid,
          'langcode' => $langcode,
        ]);

        // Delete all other alias than the actual one.
        $keep_first = TRUE;
        foreach ($alias_objects as $alias_object) {
          if ($alias_object->get('alias')->value == $actual_alias) {
            if ($keep_first) {
              $keep_first = FALSE;
              continue;
            }
            else {
              $alias_object->delete();
            }
          }
        }
      }
    }
    $this->logger()->success(dt('Duplicate Aliases removed.'));
  }

  /**
   * Replace Strings in Metatags.
   *
   * @command drush_tools:replace-string-in-metatags
   * @aliases rsimt
   *
   * @param $search
   *               String to search in the metatag-Field for all content types
   *               in all translations.
   * @param $replace
   *               String to replace in the metatag-Field for all content types
   *               in all translations.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function replaceInMetatags($search, $replace) {
    $database = \Drupal::database();

    $langcodes = \Drupal::languageManager()->getLanguages();
    $langcodesList = array_keys($langcodes);
    foreach ($langcodesList as $langcode) {

      $db_query = $database->select('node__field_meta_tags');
      $db_query->condition('field_meta_tags_value', "%gdts.one%", "LIKE");
      $db_query->condition('langcode', $langcode, '=');
      $db_query->addField('node__field_meta_tags', 'entity_id');
      $db_nids = $db_query->execute();
      foreach ($db_nids as $dbNid) {
        $nid = $dbNid->entity_id;
        $node = \Drupal\node\Entity\Node::load($nid);
        if ($node->hasTranslation($langcode)) {
          $node = $node->getTranslation($langcode);
          $serialized_tags = $node->get('field_meta_tags')->getValue();
          $replaced = $this->replace_in_serialized(
            $search,
            $replace,
            $serialized_tags[0]['value']
          );
          $replaced = serialize($replaced);
          $node->set('field_meta_tags', $replaced);
          $node->save();
        }
      }
    }
  }

  function replace_in_serialized($search, $replace, $data) {
    $unserialized_data = unserialize($data);
    if (is_array($unserialized_data)) {
      foreach ($unserialized_data as $index => $value) {
        if (is_array($value)) {
          foreach ($value as $index => $record) {
            $value[$index] = $this->replace_in_serialized(
              $search,
              $replace,
              $record
            );
          }
        }
        else {
          $unserialized_data[$index] = $this->replace_in_serialized(
            $search,
            $replace,
            $value
          );
        }
      }
    }
    else {
      $unserialized_data = str_ireplace($search, $replace, $data);
    }
    return $unserialized_data;
  }

}
